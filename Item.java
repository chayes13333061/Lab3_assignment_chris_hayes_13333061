package ie.ucd.luggage;

/**
 * Interface representing the luggage item
 * 
 * @author liliana
 *
 */
public interface Item {

	String getType();

	double getWeight();

	boolean isDangerous();

}