package ie.ucd.luggage;

public class Bomb implements Item {

	private String type;
	private double weight;
	private boolean dangerous;

	// Initialise Bomb with type of object, weight and dangerous
	public Bomb() {
		type = "Bomb";
		weight = 5.0;
		dangerous = true;
	}

	public String getType() {
		return type;
	}

	public double getWeight() {
		return weight;
	}

	public boolean isDangerous() {
		return dangerous;
	}

}
