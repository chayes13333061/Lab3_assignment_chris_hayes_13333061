package ie.ucd.luggage;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.List;

public abstract class SafeLuggage extends Luggage {

	private int password; // Password to add/remove objects

	Scanner sc = new Scanner(System.in); // Scanner to take user input

	// Initialise SafeLuggage with a password and items array
	public SafeLuggage() {
		super();
		password = 1234;
	}

	// Override add method from Luggage
	public void add(Item item) {
		System.out.println("Enter Password: "); // Ask user for password

		// Checks if user input the same as password, if yes then item is added
		if (sc.nextInt() == password) {
			items.add(item);
			System.out.println(item.getType() + " Item added\n");
		}
	}

	// Override removeItem method from Luggage
	public void removeItem(int index) {
		System.out.println("Enter Password: "); // Ask user for password

		// Checks if user input is the same as the password, if yes then an item is
		// removed
		if (sc.nextInt() == password) {
			items.remove(index);
			System.out.println("Item removed \n");
		}
	}

}
