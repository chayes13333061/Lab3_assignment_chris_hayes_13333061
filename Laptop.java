package ie.ucd.luggage;

public class Laptop implements Item {

	private String type;
	private double weight;
	private boolean dangerous;

	// Constructor that initialises the Laptop with the type of object, weight and
	// determines if
	// it is dangerous
	public Laptop() {
		type = "Laptop";
		weight = 7.0;
		dangerous = false;
	}

	public String getType() {
		return type;
	}

	public double getWeight() {
		return weight;
	}

	public boolean isDangerous() {
		return dangerous;
	}
}