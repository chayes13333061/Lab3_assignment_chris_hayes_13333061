package ie.ucd.luggage;

public class DesignerPen extends Pen {

	private String brand;

	// Constructor takes a string argument to set the brand of the pen
	public DesignerPen(String my_brand) {
		super();
		brand = my_brand;
	}

	// Overrides getType class in Pen to return brand of the pen and the string
	// "pen" after
	public String getType() {
		return brand + " pen";
	}

}
