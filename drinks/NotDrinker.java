package ie.ucd.drinks;

import ie.ucd.people.*;
import ie.ucd.items.*;

// Class that implements notDrinker, notDrinker should only drink non
// alcoholic drinks.
public class NotDrinker extends Person {

	public NotDrinker() {
		super();
	}

	// Boolean method drink that checks whether the drink is alcoholic or not.
	// If it is alcoholic then the notDrinker object will not drink it (return
	// false)
	// If it is non alcoholic the notDrinker object will drink it (return true)
	public boolean drink(Drink drink) {
		if (drink instanceof AlcoholicDrink)  
			return false;
		else
			return true;
	}

	// Returns true when the notDrinker eats
	public boolean eat(Food food) {
		return true;
	}

}
