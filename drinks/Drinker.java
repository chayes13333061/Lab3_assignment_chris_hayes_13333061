package ie.ucd.drinks;

import ie.ucd.people.*;
import ie.ucd.items.*;

// Class that implements drinker. A drinker object has an integer number 
// of alcoholic drinks that the drinker has drank, the number of drinks
// is incremented when the drinker has an alcoholic drink. Boolean method
// isDrunk determines whether the drinker is drunk or not.
public class Drinker extends Person {

	private int numberOfDrinks;

	public Drinker(double weight) {  
		super();
		super.setWeight(weight);
		numberOfDrinks = 0; // Number of drinks starts at 0
	}

	// Checks whether drink is alcoholic, if so, numberOfDrinks increments
	public boolean drink(Drink drink) {
		if (drink instanceof AlcoholicDrink) //
			numberOfDrinks++; //
		return true; // Returns true to say the drinker drank the alcoholic drink
	}

	// If the numberofDrinks is greater than the drinkers weight divided by 10, then
	// the drinker is drunk
	public boolean isDrunk() {
		if (numberOfDrinks > (super.getWeight() / 10)) {
			return true;
		} else
			return false;
	}

	// Boolean that returns true when the drinker eats
	public boolean eat(Food food) {
		return true;
	}
}
