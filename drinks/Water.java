package ie.ucd.drinks;

import ie.ucd.items.*;

// Water class that is an non alcoholic drink, takes the name of the drink and the volume
public class Water extends NotAlcoholicDrink {

	public Water(String name, double volume) {
		super(name, volume);  
	}
}
