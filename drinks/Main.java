package ie.ucd.drinks;

import ie.ucd.items.*;

public class Main {

	public static void main(String[] args) {

		Drinker drinker = new Drinker(60.0);
		NotDrinker notDrinker = new NotDrinker();

		// Create a wine object that is alcoholic (and delicious)
		Wine myWine = new Wine("Amarone", 250.0, 15.0, WineType.Red);

		// Prints true if drinker drank the drink or false if not
		System.out.println("Drinker drinks alcohol: " + drinker.drink(myWine));
		System.out.println("Non-Drinker drinks alcohol: " + notDrinker.drink(myWine));

		System.out.println("---------------------"); // Improve output readability

		System.out.println("Drinker is drunk: " + drinker.isDrunk());
		System.out.println("Drink 6 more wines.");

		// Drink 6 more wines
		drinker.drink(myWine);
		drinker.drink(myWine);  
		drinker.drink(myWine);
		drinker.drink(myWine);
		drinker.drink(myWine);
		drinker.drink(myWine);

		System.out.println("Drinker is drunk: " + drinker.isDrunk());

		System.out.println("---------------------");

		// Create a water object that is non alcoholic
		Water myWater = new Water("myWater", 500.0);

		// Prints true if notDrinker drank the drink or false if not
		System.out.println("Drinker drinks water: " + drinker.drink(myWater));
		System.out.println("Non-Drinker drinks water: " + notDrinker.drink(myWater));

	}
}
