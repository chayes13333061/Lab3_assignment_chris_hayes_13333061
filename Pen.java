package ie.ucd.luggage;

public class Pen implements Item {

	private String type;
	private double weight;
	private boolean dangerous;

	// Initialise type, weight and dangerous with hard coded values
	public Pen() {
		type = "Pen";
		weight = 0.5;
		dangerous = false;
	}

	public String getType() {
		return type;
	}

	@Override
	public double getWeight() {
		return weight;
	}

	@Override
	public boolean isDangerous() {
		return dangerous;
	}

}
