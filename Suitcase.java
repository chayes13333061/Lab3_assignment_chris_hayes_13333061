package ie.ucd.luggage;

public class Suitcase extends SafeLuggage {

	private double bagWeight;
	private double maxWeight;

	// Constructor for suitcase that assigns the the bag weight and the maximum
	// weight allowed
	public Suitcase(double my_bagWeight) {
		bagWeight = my_bagWeight;
		maxWeight = 30;
	}

	public double getBagWeight() {
		return bagWeight;
	}

	public double getMaxWeight() {
		return maxWeight;
	}

}
