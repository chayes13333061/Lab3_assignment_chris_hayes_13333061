package ie.ucd.luggage;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		// Instatiate suitcase object with a weight, Suitcase class implements
		// SafeLuggage
		Suitcase mySuitcase = new Suitcase(5.0);

		// Initialise all items
		Bomb myBomb = new Bomb();
		DesignerPen myDesignerPen = new DesignerPen("Gucci");
		Pen myPen = new Pen();
		Laptop myLaptop = new Laptop();

		// Add each item to list and then print weight
		mySuitcase.add(myLaptop);

		System.out.println("Items weight: " + mySuitcase.getWeight());

		mySuitcase.add(myPen);

		System.out.println("Items weight: " + mySuitcase.getWeight());

		// Remove items and print weight
		mySuitcase.removeItem(0);

		System.out.println("Items weight: " + mySuitcase.getWeight());

		mySuitcase.removeItem(0);

		System.out.println("Items weight: " + mySuitcase.getWeight());

	}

}
